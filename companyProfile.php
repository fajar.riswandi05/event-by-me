<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>Profile Company</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Companies</a></li>
                        <li><a href="#">Profile company</a></li>
                    </ul>
                </div>
            </div>

            <div class="component">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card h-90-vh profileCompany">
                            <div class="headerCard">
                                <img src="assets/img/headerCompanyProfile.png" class="headerImg" alt="">
                            </div>

                            <div class="row contentCompany">
                                <div class="col-md-4 col-sm-12">
                                    <div class="card boxProfileCompany">
                                        <div class="logoCompany">
                                            <img src="assets/img/company/3.png" class="img-fluid" alt="">
                                        </div>
                                        <ul class="listIconLabel mt-5">
                                            <li>
                                                <i data-feather="briefcase"></i> PT Armadius Technology
                                            </li>
                                            <li>
                                                <i data-feather="award"></i> Information Technology
                                            </li>
                                            <li>
                                                <i data-feather="mail"></i> 021 (5647 1172)
                                            </li>
                                            <li>
                                                <i data-feather="phone"></i> care@armadius.com
                                            </li>
                                            <li>
                                                <i data-feather="map-pin"></i> Jalan Ujung Pandang stlh Green Boulevard 78115, Pontianak.
                                            </li>
                                            <li><a href="#" class="btn btn-outline-primary btn-block mt-5">Edit profile</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-12">
                                    <div class="aboutCompany">
                                        <h1>We make strategies, design & development to create valuable products.</h1>
                                        <p>Regulatory oneven an enterprises such she and the got the did attributing and pushed. We currently have 36 active media campaigns across 24 projects.</p>
                                        <p>A higher spacing and movements through an impactful email campaigns. Creating a portfolio budgeting in a real time planning and phasing.</p>
                                        <p>Spaces of each debt in the digital world can help you with overall simplest authentic. Get an utilized of structure to tackle complex issues and changes.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>