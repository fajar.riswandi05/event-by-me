<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>Add Event</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Management event</a></li>
                        <li><a href="#">Add Event</a></li>
                    </ul>
                </div>
            </div>

            <div class="component">
                <form>
                <div class="card heightDefaultComponent shadow-sm">
                    <div class="row layoutFormEvent">
                        
                        <div class="col-md-8 col-sm-12 p-4">
                            <h5 class="mb-4">Complete the data</h5>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <input type="text" class="form-control mb-3" placeholder="Event name">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <select class="custom-select mb-3">
                                        <option selected>Organization</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="email" class="form-control mb-3" placeholder="Event orgenizer email">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="number" class="form-control mb-3" placeholder="Phone">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Start event date" data-provide="datepicker">
                                        <div class="input-group-append">
                                            <span class="input-group-text append-white"><i data-feather="calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="End event date" data-provide="datepicker">
                                        <div class="input-group-append">
                                            <span class="input-group-text append-white"><i data-feather="calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="text" class="form-control mb-3" placeholder="Event location address">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="text" class="form-control mb-3" placeholder="Capacity">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-check inputRadio mb-3">
                                        <input class="form-check-input" type="radio" name="regRequired" id="regRequired" value="option1">
                                        <label class="form-check-label" for="regRequired">
                                            User is required to register
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-check inputRadio mb-3">
                                        <input class="form-check-input" type="radio" name="regNotRequired" id="regNotRequired" value="option2">
                                        <label class="form-check-label" for="regNotRequired">
                                            User are not required to register
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="text" class="form-control mb-3" placeholder="Url youtube video">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="text" class="form-control mb-3" placeholder="Set domain event">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 p-4">
                            <h5 class="mb-4">Choose template</h5>
                            <div class="listTemplate row">
                                
                                <div class="col-md-6 col-sm-12">
                                    <label class="item">
                                        <input type="checkbox" name="template1" value="">
                                        <img src="assets/img/templates/1.png">
                                        
                                        <div class="setting">
                                            <a href="settingTemplate.php"><i data-feather="settings"></i> Setting</a>
                                        </div>
                                    </label>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <label class="item">
                                        <input type="checkbox" name="template2" value="">
                                        <img src="assets/img/templates/2.png">

                                        <div class="setting">
                                            <a href="settingTemplate.php"><i data-feather="settings"></i> Setting</a>
                                        </div>
                                    </label>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <label class="item">
                                        <input type="checkbox" name="template3" value="">
                                        <img src="assets/img/templates/3.png">

                                        <div class="setting">
                                            <a href="settingTemplate.php"><i data-feather="settings"></i> Setting</a>
                                        </div>
                                    </label>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <label class="item">
                                        <input type="checkbox" name="template4" value="">
                                        <img src="assets/img/templates/4.png">

                                        <div class="setting">
                                            <a href="settingTemplate.php"><i data-feather="settings"></i> Setting</a>
                                        </div>
                                    </label>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <label class="item">
                                        <input type="checkbox" name="template5" value="">
                                        <img src="assets/img/templates/5.png">

                                        <div class="setting">
                                            <a href="settingTemplate.php"><i data-feather="settings"></i> Setting</a>
                                        </div>
                                    </label>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <label class="item">
                                        <input type="checkbox" name="template6" value="">
                                        <img src="assets/img/templates/6.png">

                                        <div class="setting">
                                            <a href="settingTemplate.php"><i data-feather="settings"></i> Setting</a>
                                        </div>
                                    </label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="#" class="btn btn-sm btn-outline-primary mr-2">Cancel</a>
                        <a href="#" id="saveData" class="btn btn-sm btn-primary">Save event</a>
                    </div>
                </div>
                </form>
            </div>

            

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>