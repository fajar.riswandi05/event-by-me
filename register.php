<?php include_once "header.php"; ?>
    <div id="loginRegister">
        
        <div class="content">
            <a href="#" class="titleLoginRegister"><img src="assets/img/logo-lg.svg" alt="logo"></a>
            <div class="card shadow-sm">
                <div class="card-body p-5">
                    
                    <h3 class="mb-4 ml-auto mr-auto d-table mt-0">Register</h3>
                    
                    <form action="#" method="post">

                        <div class="row">
                            <div class="col col-md-6 col-sm-12">
                                <input type="text" class="form-control mb-3" placeholder="Username">
                            </div>
                            <div class="col col-md-6 col-sm-12">
                                <input type="text" class="form-control mb-3" placeholder="Organization">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-md-6 col-sm-12">
                                <input type="email" class="form-control mb-3" placeholder="Email">
                            </div>
                            <div class="col col-md-6 col-sm-12">
                                <input type="password" class="form-control mb-3" placeholder="Password">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-md-6 col-sm-12">
                                <input type="text" class="form-control mb-3" placeholder="Sex">
                            </div>
                            <div class="col col-md-6 col-sm-12">
                                <input type="text" class="form-control mb-3" placeholder="Date of birth">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-md-6 col-sm-12">
                                <input type="number" class="form-control mb-3" placeholder="Phone">
                            </div>
                            <div class="col col-md-6 col-sm-12">
                                <input type="text" class="form-control mb-3" placeholder="Address">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col"><button type="button" onclick="window.location='login.php';" class="btn btn-outline-primary btn-block">Login</button></div>
                            <div class="col"><button type="button" onclick="window.location='dashboard.php';" class="btn btn-primary btn-block">Register</button></div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>

    </div>
<?php include_once "footer.php"; ?>