
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/feather.min.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="assets/css/bootstrap-datepicker.min.css"/>
<script src="assets/js/sweetalert.min.js"></script>

<script>
// Alert Modal Type
    $(document).on('click', '#saveData', function(e) {
        swal(
            'Success',
            'Data saved successfully!',
            'success'
        )
    });

    $(document).on('click', '#error', function(e) {
        swal(
            'Error!',
            'You clicked the <b style="color:red;">error</b> button!',
            'error'
        )
    });
    
// Alert With Input Type
$(document).on('click', '#deleteConfirmation', function(e) {
    swal({
        title: "Are you sure?",
		text: "You will not be able to recover this imaginary file!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Yes, delete it!',
		closeOnConfirm: false,
        
    })
});

</script>

<script>
    // Select report event
    $(document).ready(function() {
        $('.selectReportEvent').select2({
            placeholder: "Slect report",
            allowClear: true
        });
    });
</script>

<script>
    // Datepicker
    $('.datepicker').datepicker();
</script>

<script> 
    // Feather icon default
    feather.replace() 
</script>

<script>
    // Setting costume color
    $("#mainColor").spectrum({ color: "#fff" });
    $("#secondaryColor").spectrum({ color: "#fff" });
    $("#menuColor").spectrum({ color: "#fff" });
</script>

<script>
    // Buttno back in secondary menu type
    function goBack() {
    window.history.back();
    }
</script>

<script>
    // Toggle button mobile trigger 
    $(document).ready(function(){
    $('.open-button').click(function(){
    $(this).toggleClass('open');
    });

    /* Menu fade/in out on mobile */
    $(".open-button").click(function(e){
        e.preventDefault();
        $("#sidebarLeft").toggleClass('open');
    });

    });

    /* Close menu using the escape key */
    $(document).keyup(function (e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
        if ($("#sidebarLeft").hasClass("open")) {
            $("#sidebarLeft").removeClass("open")
        }

        if ($(".open-button").hasClass("open")) {
            $(".open-button").removeClass("open")
        }
    
        // if ($("body").hasClass("lock-scroll")) {
        //     $("body").removeClass("lock-scroll")
        // }
    }
    });
    $('.open-button').click(function () {

    if ($(this).hasClass('open')) {
        lockScroll(false);
    } else {
        lockScroll(true);
    }
    });

    function lockScroll(shouldLock) {

    if (shouldLock === undefined) {
        shouldLock = true;
    }

    if (shouldLock) {
        if (!$('body').hasClass('lock-scroll')) {
            $('body').addClass('lock-scroll');
        }
    } else {
        if ($('body').hasClass('lock-scroll')) {
            $('body').removeClass('lock-scroll');
        }
    }
}
</script>
</body>
</html>