<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>Add Company</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Management Company</a></li>
                        <li><a href="#">Add Company</a></li>
                    </ul>
                </div>
            </div>

            <div class="component">
                <form>
                <div class="card heightDefaultComponent shadow-sm">
                    <div class="row layoutFormEvent">
                        
                        <div class="col-md-8 col-sm-12 p-4">
                            <h5 class="mb-4">Complete the data</h5>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <input type="text" class="form-control mb-3" placeholder="Company Name">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <select class="custom-select mb-3">
                                        <option selected>Category Company</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="email" class="form-control mb-3" placeholder="Email">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <input type="number" class="form-control mb-3" placeholder="Phone">
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group mb-3">
                                        <label for="address">Address company</label>
                                        <textarea class="form-control" id="address" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div id="editor">
                                        
                                    </div>
                                    <script>
                                        ClassicEditor
                                        .create( document.querySelector( '#editor' ) )
                                        .then( editor => {
                                            console.log( editor );
                                        } )
                                        .catch( error => {
                                            console.error( error );
                                        } );
                                        
                                    </script>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 p-4">
                            <h5 class="mb-4">Choose template</h5>
                            <div class="uploadImage mb-3">
                                <input type="file" class="inputImageOrFile" name="uploadImage1" id="uploadImage1" />
                                <label for="uploadImage1 mb-3">
                                    <i data-feather="image"></i>
                                    <span>Upload logo</span>
                                </label>
                            </div>
                            <div class="uploadImage mb-3">
                                <input type="file" class="inputImageOrFile" name="uploadImage2" id="uploadImage2" />
                                <label for="uploadImage2 mb-3">
                                    <i data-feather="image"></i>
                                    <span>Upload Banner profile</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="#" class="btn btn-sm btn-outline-primary mr-2">Cancel</a>
                        <a href="#" id="saveData" class="btn btn-sm btn-primary">Save Company</a>
                    </div>
                </div>
                </form>
            </div>

            

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>