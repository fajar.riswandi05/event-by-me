<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>List Users</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Management Users</a></li>
                    </ul>
                </div>
            </div>

            <div class="component">
                <div class="title mb-3">
                    <div class="d-flex">
                        <div class="dropdown mr-3">
                            <button class="btn btn-light btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Select All</a>
                                <a class="dropdown-item" href="#">Uncheck All</a>
                                <a class="dropdown-item" href="#">Delete</a>
                            </div>
                        </div>
                            <a href="addUser.php" class="btn btn-sm btn-outline-primary">Add <i data-feather="plus"></i></a>
                    </div>

                    <div>
                        <div class="form-group searchInput mb-0 mt-0">
                            <input class="form-control" type="text" placeholder="Search">
                        </div>
                    </div>
                </div>
                <div class="card heightDefaultComponent shadow-sm">
                    <div class="table-responsive">
                        <table class="table cardTable table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <tr>
                                    <td>
                                        <img src="assets/img/users/1.png" alt="" class="thumbnailSmall">
                                        <span>Grace jhonson</span>
                                    </td>
                                    <td>grace.j@gmail.com</td>
                                    <td>0198 1241 123</td>
                                    <td><span class="badge badge-success">Active</span></td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#detailUserModal"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="assets/img/users/2.png" alt="" class="thumbnailSmall">
                                        <span>Grace jhonson</span>
                                    </td>
                                    <td>grace.j@gmail.com</td>
                                    <td>0198 1241 123</td>
                                    <td><span class="badge badge-success">Active</span></td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#detailUserModal"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="assets/img/users/3.png" alt="" class="thumbnailSmall">
                                        <span>Grace jhonson</span>
                                    </td>
                                    <td>grace.j@gmail.com</td>
                                    <td>0198 1241 123</td>
                                    <td><span class="badge badge-secondary">Non active</span></td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#detailUserModal"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="assets/img/users/4.png" alt="" class="thumbnailSmall">
                                        <span>Grace jhonson</span>
                                    </td>
                                    <td>grace.j@gmail.com</td>
                                    <td>0198 1241 123</td>
                                    <td><span class="badge badge-success">Active</span></td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#detailUserModal"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="assets/img/users/5.png" alt="" class="thumbnailSmall">
                                        <span>Grace jhonson</span>
                                    </td>
                                    <td>grace.j@gmail.com</td>
                                    <td>0198 1241 123</td>
                                    <td><span class="badge badge-success">Active</span></td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#detailUserModal"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="assets/img/users/6.png" alt="" class="thumbnailSmall">
                                        <span>Grace jhonson</span>
                                    </td>
                                    <td>grace.j@gmail.com</td>
                                    <td>0198 1241 123</td>
                                    <td><span class="badge badge-success">Active</span></td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#detailUserModal"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>

                        <nav aria-label="Page navigation">
                            <ul class="pagination  ml-3">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>

            <!-- Detail user modal -->
                <!-- Modal -->
                <div class="modal fade" id="detailUserModal" tabindex="-1" aria-labelledby="detailUserModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="detailUserModalLabel">User profile</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="profileContent">
                        <div class="boxProfile pb-5">
                            <img src="assets/img/profile.png" class="img-fluid" alt="">
                            <h3>Ghany</h3>
                            <table class="table table-striped table-sm">
                                <tbody>
                                    <tr>
                                        <td>Username</td>
                                        <td>ahmed</td>
                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td>Ahmed Sodiq</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>ahmed.so@gmail.com</td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td>+62 812 8829 22xx</td>
                                    </tr>
                                    <tr>
                                        <td>Organization</td>
                                        <td>PT Armadius Teknologi</td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td>Man</td>
                                    </tr>
                                    <tr>
                                        <td>Date of birth</td>
                                        <td>25-05-1994</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>Active</td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>Jl Jendral Sudirman, Jakarta pusat  14045</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="d-flex">
                                <a href="tel:08982278xxxx" class="btn btn-outline-primary d-table"><i data-feather="phone" class="mr-2"></i> Call me</a>
                                <a href="mailto:email@email.com" class="btn btn-outline-primary d-table"><i data-feather="mail" class="mr-2"></i> Send message</a>
                            </p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <!-- Detail user modal -->

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>