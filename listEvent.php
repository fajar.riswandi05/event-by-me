<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>List Event</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Management event</a></li>
                        <li><a href="#">List Event</a></li>
                    </ul>
                </div>
            </div>

            <div class="component">
                <div class="title mb-3">
                    <div class="d-flex">
                        <div class="dropdown mr-3">
                            <button class="btn btn-light btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Select All</a>
                                <a class="dropdown-item" href="#">Uncheck All</a>
                                <a class="dropdown-item" href="#">Delete</a>
                            </div>
                        </div>
                            <a href="addEvent.php" class="btn btn-sm btn-outline-primary">Add <i data-feather="plus"></i></a>
                    </div>

                    <div>
                        <div class="form-group searchInput mb-0 mt-0">
                            <input class="form-control" type="text" placeholder="Search">
                        </div>
                    </div>
                </div>
                <div class="card heightDefaultComponent shadow-sm">
                    <div class="table-responsive">
                        <table class="table cardTable table-striped">
                            <thead>
                                <tr>
                                    <th>Event name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="assets/img/company/1.png" alt="" class="thumbnailSmall">
                                        <span>Event pembukaan cabang baru jakarta</span>
                                    </td>
                                    <td>12-Des-2020</td>
                                    <td>15-Des-2020</td>
                                    <td>Active</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="assets/img/company/2.png" alt="" class="thumbnailSmall">
                                        <span>Event pembukaan cabang baru jakarta</span>
                                    </td>
                                    <td>12-Des-2020</td>
                                    <td>15-Des-2020</td>
                                    <td>Active</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="assets/img/company/3.png" alt="" class="thumbnailSmall">
                                        <span>Event pembukaan cabang baru jakarta</span>
                                    </td>
                                    <td>12-Des-2020</td>
                                    <td>15-Des-2020</td>
                                    <td>Active</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="assets/img/company/5.png" alt="" class="thumbnailSmall">
                                        <span>Event pembukaan cabang baru jakarta</span>
                                    </td>
                                    <td>12-Des-2020</td>
                                    <td>15-Des-2020</td>
                                    <td>Active</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="assets/img/company/6.png" alt="" class="thumbnailSmall">
                                        <span>Event pembukaan cabang baru jakarta</span>
                                    </td>
                                    <td>12-Des-2020</td>
                                    <td>15-Des-2020</td>
                                    <td>Active</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="assets/img/company/4.png" alt="" class="thumbnailSmall">
                                        <span>Event pembukaan cabang baru jakarta</span>
                                    </td>
                                    <td>12-Des-2020</td>
                                    <td>15-Des-2020</td>
                                    <td>Active</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="eye"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="edit"></i></a>
                                        <a href="#" class="btn btn-outline-primary btn-sm"><i data-feather="trash-2"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <nav aria-label="Page navigation">
                            <ul class="pagination  ml-3">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>