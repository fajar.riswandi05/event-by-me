<!-- Sidebar left -->
<div id="sidebarLeft">
    <div class="headNav mb-4 ">
        <a href="#" class="logo"><img src="assets/img/logo.svg" alt="Logo"></a>
    </div>

    <div class="content">
        <div class="info primary mb-5">
            <i data-feather="heart"></i> Hi <strong>Ghany</strong>, Welcome back!
        </div>

        <ul class="navDesktop mb-5">
            <li><a href="dashboard.php" class="active"><i data-feather="grid"></i> Dashboard</a></li>
            <li><a href="manageEvent.php"><i data-feather="star"></i> Manage Event</a></li>
            <li><a href="myProfile.php"><i data-feather="user"></i> My Profile</a></li>
            <li><a href="listCompanies.php"><i data-feather="briefcase"></i> Companies</a></li>
            <li><a href="listUser.php"><i data-feather="users"></i> User</a></li>
        </ul>

        <div class="documentation">
            <img src="assets/img/documentation.svg" alt="">
        </div>

    </div>

</div>
<!-- Sidebar left -->