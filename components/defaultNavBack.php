<!-- Default nav -->
<div class="headNav mb-3 defaultNav bgBody">
    <div>
        <button onclick="goBack()" class="btn btn-sm mt-2 btn-primary"> <i data-feather="arrow-left"></i> Back</button>
    </div>
    <div>
        <ul class="navUser">
            <li>
                <a href="#" class="item darkmode"><i data-feather="moon"></i></a>
            </li>
            <li>
                <a href="#" class="item notification">
                    <i data-feather="bell"></i>
                    <span></span>
                    
                    <div class="listNotification">
                        <ul>
                            <li class="item header">
                                <h5>Notification</h5>
                            </li>
                            <li class="item">
                                Pesan baru untuk kamu
                                <span class="badge badge-primary badge-pill">14</span>
                            </li>
                            <li class="item">
                                Kamu perlu baca ini
                                <span class="badge badge-primary badge-pill">4</span>
                            </li>
                            <li class="item">
                                Kamu perlu baca ini
                                <span class="badge badge-primary badge-pill">4</span>
                            </li>
                            <li class="item">
                                Kamu perlu baca ini
                                <span class="badge badge-primary badge-pill">4</span>
                            </li>
                        </ul>
                    </div>

                </a>

            </li>
            <li>
                <a href="#" class="item user">
                    <span class="mr-2">Ghany</span>
                    <img src="assets/img/avatar.png" alt="Avatar">
                </a>
            </li>
        </ul>

        <!-- Mobile nav triger -->
        <a href="#" class="open-button">
            <span></span>
            <span></span>
            <span></span>
        </a>
        <!-- Mobile nav triger -->

    </div>
</div>
<!-- Default nav -->