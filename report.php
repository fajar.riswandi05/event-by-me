<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb w-100 justify-content-between">
                <div>
                    <h5>Event Overview</h5>
                </div>
                <div>
                    <select class="selectOption2 selectReportEvent" name="state">
                        <option value="1">All</option>
                        <option value="2">Event Surabaya</option>
                        <option value="2">Event Jakarta</option>
                        <option value="2">Event Bandung</option>
                    </select>
                </div>
            </div>

            <div class="component reportEvent">
                <!-- Summary -->
                <div class="row">

                        <div class="col-md-3 col-sm-12">
                            <div class="card mb-3 summary primary">
                                <div class="contentBox">
                                    <i data-feather="activity"></i>
                                    <span>Total Activity</span>
                                    <h4>1982</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="card mb-3 summary">
                                <div class="contentBox">
                                    <i data-feather="briefcase"></i>
                                    <span>Sponsor</span>
                                    <h4>33</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="card mb-3 summary">
                                <div class="contentBox">
                                    <i data-feather="airplay"></i>
                                    <span>Page Visit</span>
                                    <h4>2214</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="card mb-3 summary">
                                <div class="contentBox">
                                    <i data-feather="users"></i>
                                    <span>Total Member</span>
                                    <h4>215</h4>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- Summary -->

                <!-- column -->
                <div class="row">
                    <!-- Trend -->
                    <div class="col-md-8 col-sm-12">
                        <div class="card h-65">
                            <div class="card-body">
                                <h5>Visitor Trend</h5>
                                <div id="chartTrend"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Trend -->

                    <!-- Conversion -->
                    <div class="col-md-4 col-sm-12">
                        <div class="card h-65">
                            <div class="card-body">
                                <h5>Conversion</h5>
                                <div class="table-responsive">
                                    <table class="table table-striped table-sm">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Page</th>
                                                <th class="text-center">Conversion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                            <tr>
                                                <td>12-12-2020</td>
                                                <td>Contact Us</td>
                                                <td class="text-center">22</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Conversion -->
                </div>
                <!-- column -->
            

        </div>

    </div>
    <!-- Main Content -->

</div>

<script>
    var options = {
          series: [{
          name: 'Visitor',
          data: [31, 40, 28, 51, 42, 109, 100]
        }, {
          name: 'Pages',
          data: [11, 32, 45, 32, 34, 52, 41]
        }],
          colors:['#613CCC', '#F6B756'],
          chart: {
            height: 450,
            type: 'area',
            toolbar:{
                show:false
            }
        },
        fill: {
            colors:['#613CCC', '#F6B756'],
            opacity: 0.1,
            type: 'solid',
        },
        legend: {
            show: true,
            showForSingleSeries: false,
            showForNullSeries: true,
            showForZeroSeries: true,
            position: 'bottom',
            offsetX: 0,
            offsetY: 5,
            labels: {
                colors:['#613CCC', '#F6B756'],
                useSeriesColors: false
            }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'smooth',
          colors:['#613CCC', '#F6B756']
        },
        xaxis: {
          type: 'datetime',
          categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
        },
        tooltip: {
          x: {
            format: 'dd/MM/yy HH:mm'
          },
        },
        };

        var chart = new ApexCharts(document.querySelector("#chartTrend"), options);
        chart.render();
</script>
<!-- Layout -->
<?php include_once "footer.php"; ?>