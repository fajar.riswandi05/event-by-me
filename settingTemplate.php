<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <h4>Setting template</h4>
            </div>

            <div class="component">
                <div class="card cardTransparent">
                    <div class="row heightDefaultComponent">
                        <div class="col-md-3 col-sm-12">
                            <h5>Upload image</h5>

                            <div class="uploadImage mb-3">
                                <input type="file" class="inputImageOrFile" name="uploadImage1" id="uploadImage1" />
                                <label for="uploadImage1 mb-3">
                                    <i data-feather="image"></i>
                                    <span>Upload logo</span>
                                </label>
                            </div>

                            <div class="uploadImage mb-3">
                                <input type="file" class="inputImageOrFile" name="uploadImage2" id="uploadImage2" />
                                <label for="uploadImage2">
                                    <i data-feather="image"></i>
                                    <span>Upload logo</span>
                                </label>
                            </div>

                            <div class="uploadImage mb-3">
                                <input type="file" class="inputImageOrFile" name="uploadImage3" id="uploadImage3" />
                                <label for="uploadImage3">
                                    <i data-feather="image"></i>
                                    <span>Upload Favicon</span>
                                </label>
                            </div>

                            <div class="uploadImage mb-3">
                                <input type="file" class="inputImageOrFile" name="uploadImage4" id="uploadImage4" />
                                <label for="uploadImage4">
                                    <i data-feather="image"></i>
                                    <span>Upload banner (1400 x 900 px)</span>
                                </label>
                            </div>

                            <div class="uploadImage mb-3">
                                <input type="file" class="inputImageOrFile" name="uploadImage5" id="uploadImage5" />
                                <label for="uploadImage5">
                                    <i data-feather="image"></i>
                                    <span>Upload banner (900 x 900 px)</span>
                                </label>
                            </div>

                        </div>

                        <div class="col-md-3 col-sm-12">
                            <!-- Main Color -->
                            <h5>Main Color</h5>
                            <div class="colorLists mb-3">
                                <div class="item">
                                    <input type="radio" id="itemColor">
                                    <label for="itemColor" class="primary"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemColor2">
                                    <label for="itemColor2" class="yellow"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemColor3">
                                    <label for="itemColor3" class="pink"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemColor4">
                                    <label for="itemColor4" class="lightBlue"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemColor5">
                                    <label for="itemColor5" class="black"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemColor6">
                                    <label for="itemColor6" class="secondaryBlack"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemColor7">
                                    <label for="itemColor7" class="red"></label>
                                </div>
                                <div class="item">
                                    <input type='text' id="mainColor" />
                                </div>
                            </div>
                            <!-- Main Color -->

                            <!-- Secondary Color -->
                            <h5>Secondary Color</h5>
                            <div class="colorLists mb-3">
                                <div class="item">
                                    <input type="radio" id="itemSecondaryColor">
                                    <label for="itemSecondaryColor" class="primary"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemSecondaryColor2">
                                    <label for="itemSecondaryColor2" class="yellow"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemSecondaryColor3">
                                    <label for="itemSecondaryColor3" class="pink"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemSecondaryColor4">
                                    <label for="itemSecondaryColor4" class="lightBlue"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemSecondaryColor5">
                                    <label for="itemSecondaryColor5" class="black"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemSecondaryColor6">
                                    <label for="itemSecondaryColor6" class="secondaryBlack"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemSecondaryColor7">
                                    <label for="itemSecondaryColor7" class="red"></label>
                                </div>
                                <div class="item">
                                    <input type='text' id="secondaryColor" />
                                </div>
                            </div>
                            <!-- Secondary Color -->

                            <!-- Main Menu Color -->
                            <h5>Main Menu Color</h5>
                            <div class="colorLists mb-3">
                                <div class="item">
                                    <input type="radio" id="itemMainMenuColor">
                                    <label for="itemMainMenuColor" class="primary"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemMainMenuColor2">
                                    <label for="itemMainMenuColor2" class="yellow"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemMainMenuColor3">
                                    <label for="itemMainMenuColor3" class="pink"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemMainMenuColor4">
                                    <label for="itemMainMenuColor4" class="lightBlue"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemMainMenuColor5">
                                    <label for="itemMainMenuColor5" class="black"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemMainMenuColor6">
                                    <label for="itemMainMenuColor6" class="secondaryBlack"></label>
                                </div>
                                <div class="item">
                                    <input type="radio" id="itemMainMenuColor7">
                                    <label for="itemMainMenuColor7" class="red"></label>
                                </div>
                                <div class="item">
                                    <input type='text' id="menuColor" />
                                </div>
                            </div>
                            <!-- Main Menu Color -->

                        </div>
                        
                        <div class="col-md-6 col-sm-12">
                            <!-- Setting special page -->
                            <h5>Setting sppecial page</h5>
                            <div class="listSwitchBox">
                                
                                <div class="item mb-3">
                                    <div class="contentItem">
                                        <h5>Show page contact</h5>
                                        <p>This page contains all contact information related to the event organizer</p>
                                    </div>    
                                    <div class="buttonSwitch">
                                        <label class="switchToggle">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="item mb-3">
                                    <div class="contentItem">
                                        <h5>Show page contact</h5>
                                        <p>This page contains all contact information related to the event organizer</p>
                                    </div>    
                                    <div class="buttonSwitch">
                                        <label class="switchToggle">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="item mb-3">
                                    <div class="contentItem">
                                        <h5>Show page contact</h5>
                                        <p>This page contains all contact information related to the event organizer</p>
                                    </div>    
                                    <div class="buttonSwitch">
                                        <label class="switchToggle">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="item mb-3">
                                    <div class="contentItem">
                                        <h5>Show page contact</h5>
                                        <p>This page contains all contact information related to the event organizer</p>
                                    </div>    
                                    <div class="buttonSwitch">
                                        <label class="switchToggle">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- Setting special page -->
                        </div>
                    </div>

                    <div class="card-footer d-flex justify-content-end">
                        <a href="#" class="btn btn-sm btn-outline-primary mr-2">Cancel</a>
                        <a href="#" class="btn btn-sm btn-primary">Apply setting</a>
                    </div>
                </div>
            </div>

            

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>