<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>My Profile</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Management Profile</a></li>
                    </ul>
                </div>
            </div>

            <div class="component myProfile">
                <div class="card h-90-vh">
                    
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active d-flex" id="myprofile-tab" data-toggle="tab" href="#myprofile" role="tab" aria-controls="myprofile" aria-selected="true"><i data-feather="user" class="mr-2"></i> My Profile</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link d-flex" id="editprofile-tab" data-toggle="tab" href="#editprofile" role="tab" aria-controls="editprofile" aria-selected="false"><i data-feather="edit-2" class="mr-2"></i> Edit Profile</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link d-flex" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="false"><i data-feather="unlock" class="mr-2"></i> Edit Password</a>
                    </li>
                </ul>

                <div class="tab-content" id="myTabContent">
                    <!-- My Profile -->
                    <div class="tab-pane fade show active" id="myprofile" role="tabpanel" aria-labelledby="myprofile-tab">
                        <div class="card-body profileContent">
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <div class="boxProfile">
                                        <img src="assets/img/profile.png" class="img-fluid" alt="">
                                        <h3>Ghany</h3>
                                        <p>I'm a designer & front end engineer, I like everything related to visual interactions and always try to be an expert in my field</p>
                                        <p><a href="tel:08982278xxxx" class="btn btn-outline-primary d-table"><i data-feather="phone" class="mr-2"></i> Call me</a></p>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-sm">
                                            <tbody>
                                                <tr>
                                                    <td>Username</td>
                                                    <td>ahmed</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>Ahmed Sodiq</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>ahmed.so@gmail.com</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>+62 812 8829 22xx</td>
                                                </tr>
                                                <tr>
                                                    <td>Organization</td>
                                                    <td>PT Armadius Teknologi</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender</td>
                                                    <td>Man</td>
                                                </tr>
                                                <tr>
                                                    <td>Date of birth</td>
                                                    <td>25-05-1994</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>Active</td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td>Jl Jendral Sudirman, Jakarta pusat  14045</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- My Profile -->

                    <!-- Edit Profile -->
                    <div class="tab-pane fade" id="editprofile" role="tabpanel" aria-labelledby="editprofile-tab">
                        <div class="card-body">
                            <h5 class="mb-3">Edit Profile</h5>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Username">
                                        <input type="text" class="form-control mb-3" placeholder="Username" value="ahmed">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Name">
                                        <input type="text" class="form-control mb-3" placeholder="Name" value="Ahmed Sodiq">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Email">
                                        <input type="email" class="form-control mb-3" placeholder="Email" value="ahmed.so@gmail.com">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Phone">
                                        <input type="number" class="form-control mb-3" placeholder="Phone" value="+62 812 8829 22xx">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Organization">
                                        <select class="custom-select mb-3">
                                            <option>Organization</option>
                                            <option value="1" selected>PT Armadius Teknologi</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Gender">
                                        <select class="custom-select mb-3">
                                            <option>Gender</option>
                                            <option value="1" selected>Man</option>
                                            <option value="2">Woman</option>
                                            <option value="3">Animal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Date of birth">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Date of birth" data-provide="datepicker" value="12/12/1994">
                                            <div class="input-group-append">
                                                <span class="input-group-text append-white"><i data-feather="calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Photo profile">
                                        <div class="custom-file mb-3">
                                        <input type="file" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Address">
                                        <textarea class="form-control" id="address" rows="3">Jl Jendral Sudirman, Jakarta pusat 14045</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-5">
                                <div class="col d-flex">
                                    <button class="btn btn-outline-primary mr-3">Clear</button>
                                    <button class="btn btn-primary">Apply change</button>
                                </div>
                            </div>

                        </div>    
                    </div>
                    <!-- Edit Profile -->

                    <!-- Password -->
                    <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
                        <div class="card-body">
                            <h5 class="mb-3">Reset password</h5>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="New password">
                                        <input type="password" class="form-control mb-3" placeholder="New password">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Repeat password">
                                        <input type="password" class="form-control mb-3" placeholder="Repeat password">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col d-flex">
                                    <button class="btn btn-outline-primary mr-3">Clear</button>
                                    <button class="btn btn-primary">Apply change</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Password -->
                </div>

                </div>
            </div>

            

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>