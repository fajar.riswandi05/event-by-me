<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/global.min.css">
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <script src="assets/js/jquery-3.5.1.slim.min.js" ></script>
    <link rel="stylesheet" href="assets/css/spectrum.css">
    <script src="assets/js/spectrum.js"></script>
    <script src="assets/js/ckeditor.js"></script>
    <script src="assets/js/apexcharts.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <title>Event By Me - Dashboard</title>
</head>
<body>