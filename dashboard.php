<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNav.php" ?>
    <?php #include_once "components/defaultNavBack.php" ?>

        <div class="content">
            
            <!-- Summary Box -->
            <div class="summaryBox row">
                <div class="col-md-4 col-sm-12 item">
                    <div class="box">
                        <div><h4>28</h4></div>
                        <div><label>All events <span>Active & nonactive</span></label></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 item">
                    <div class="box">
                        <div><h4>28</h4></div>
                        <div><label>All events <span>Active & nonactive</span></label></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 item">
                    <div class="box">
                        <div><h4>28</h4></div>
                        <div><label>All events <span>Active & nonactive</span></label></div>
                    </div>
                </div>
            </div>
            <!-- Summary Box -->

            <!-- Content Bottom -->
            <div class="row mt-3">
                <div class="col-md-8 mb-3">
                    
                <!-- List table -->
                <div class="component">
                    <div class="title">
                        <div><h5>List Events</h5></div>
                        <div><a href="#">View all <i data-feather="chevrons-right"></i></a></div>
                    </div>

                    <div class="card heightDefaultComponent shadow-sm">
                        <div class="table-responsive">
                            <table class="table cardTable table-striped">
                                <thead>
                                    <tr>
                                        <th>Event name</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src="assets/img/company/1.png" alt="" class="thumbnailSmall">
                                            <span>Event pembukaan cabang baru jakarta</span>
                                        </td>
                                        <td>12-Des-2020</td>
                                        <td>15-Des-2020</td>
                                        <td>Active</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="assets/img/company/2.png" alt="" class="thumbnailSmall">
                                            <span>Event pembukaan cabang baru jakarta</span>
                                        </td>
                                        <td>12-Des-2020</td>
                                        <td>15-Des-2020</td>
                                        <td>Active</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="assets/img/company/3.png" alt="" class="thumbnailSmall">
                                            <span>Event pembukaan cabang baru jakarta</span>
                                        </td>
                                        <td>12-Des-2020</td>
                                        <td>15-Des-2020</td>
                                        <td>Active</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="assets/img/company/5.png" alt="" class="thumbnailSmall">
                                            <span>Event pembukaan cabang baru jakarta</span>
                                        </td>
                                        <td>12-Des-2020</td>
                                        <td>15-Des-2020</td>
                                        <td>Active</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="assets/img/company/6.png" alt="" class="thumbnailSmall">
                                            <span>Event pembukaan cabang baru jakarta</span>
                                        </td>
                                        <td>12-Des-2020</td>
                                        <td>15-Des-2020</td>
                                        <td>Active</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="assets/img/company/4.png" alt="" class="thumbnailSmall">
                                            <span>Event pembukaan cabang baru jakarta</span>
                                        </td>
                                        <td>12-Des-2020</td>
                                        <td>15-Des-2020</td>
                                        <td>Active</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- List table -->

                </div>
                <div class="col-md-4">
                    <!-- List Client -->
                    <div class="component">
                        <div class="title">
                            <div><h5>List Clients</h5></div>
                            <div><a href="#">View all <i data-feather="chevrons-right"></i></a></div>
                        </div>

                        <div class="heightDefaultComponent listClient">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 mb-3">
                                    <div class="card item"><img src="assets/img/company/1.png" alt="Company"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 mb-3">
                                    <div class="card item"><img src="assets/img/company/2.png" alt="Company"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 mb-3">
                                    <div class="card item"><img src="assets/img/company/3.png" alt="Company"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 mb-3">
                                    <div class="card item"><img src="assets/img/company/4.png" alt="Company"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 mb-3">
                                    <div class="card item"><img src="assets/img/company/5.png" alt="Company"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 mb-3">
                                    <div class="card item"><img src="assets/img/company/6.png" alt="Company"></div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card shadow-sm">
                                        <img src="assets/img/whatYouCanDo.svg" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- List Client -->
                </div>
            </div>
            <!-- Content Bottom -->

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>