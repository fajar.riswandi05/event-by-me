<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>Manage Event</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Management event</a></li>
                        <li><a href="#">Main Feature Event</a></li>
                    </ul>
                </div>
            </div>

            <div class="component">
                <div class="row">

                    <div class="col-md-4 col-sm-12">
                        <div class="card h-90-vh pt-2 mb-3">
                            <img src="assets/img/preparation.svg" class="img-fluid" alt="">
                            <section class="p-4 mt-4 text-center">
                                <h3 class="text-primary">Preparation</h3>
                                <p>Prepare all think before decide to start some event. Manage all event, create new event, setup your event and many more you can do.</p>
                                <p><a href="addEvent.php" class="btn btn-outline-primary">Let's Go</a></p>
                            </section>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="card h-90-vh pt-2 mb-3">
                            <img src="assets/img/showTime.svg" class="img-fluid" alt="">
                            <section class="p-4 mt-4 text-center">
                                <h3 class="text-primary">Show Time</h3>
                                <p>Manage all think, about you event in real time, you can manage your event, change some data, or change some setting in template</p>
                                <p><a href="listEvent.php" class="btn btn-outline-primary">Let's Go</a></p>
                            </section>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="card h-90-vh pt-2  mb-3">
                            <img src="assets/img/postEvent.svg" class="img-fluid" alt="">
                            <section class="p-4 mt-4 text-center">
                                <h3 class="text-primary">Post Event</h3>
                                <p>Post event, you can see all data reporting to analyze, to know event performance, check event goal and some incident in your event</p>
                                <p><a href="report.php" class="btn btn-outline-primary">Let's Go</a></p>
                            </section>
                        </div>
                    </div>

                </div>
            </div>

            

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>