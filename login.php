<?php include_once "header.php"; ?>
    <div id="loginRegister">
        
        <div class="content">
            <a href="#" class="titleLoginRegister"><img src="assets/img/logo-lg.svg" alt="logo"></a>
            <div class="card shadow-sm">
                <div class="card-body p-5">
                    
                    <h3 class="mb-4 ml-auto mr-auto d-table mt-0">Login</h3>
                    
                    <form action="#" method="post">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend prepend-white">
                                <span class="input-group-text" id="basic-addon1"><i data-feather="user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend prepend-white">
                                <span class="input-group-text" id="basic-addon2"><i data-feather="lock"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon2">
                        </div>

                        <button type="button" class="btn btn-primary btn-block">Login</button>
                        <button type="button" class="btn btn-block btn-link">Forget your password?</button>
                        <br>
                        <button type="button" onclick="window.location='register.php';" class="btn btn-outline-primary btn-block">Register</button>

                    </form>
                </div>
            </div>
        </div>

    </div>
<?php include_once "footer.php"; ?>