<?php include_once "header.php"; ?>

<!-- Layout -->
<div id="mainLayout">
    
    <?php include_once "components/sidebarLeft.php"; ?>

    <!-- Main Content -->
    <div id="mainContent">

    <?php include_once "components/defaultNavBack.php" ?>

        <div class="content">
            <div class="titleBreadcrumb">
                <div>
                    <h5>Add User</h5>
                </div>
                <div>
                    <ul>
                        <li><a href="#">Management user</a></li>
                        <li><a href="#">Add new user</a></li>
                    </ul>
                </div>
            </div>

            <div class="component">
                <form>
                <div class="card heightDefaultComponent shadow-sm">
                    <div class="layoutFormEvent p-4">
                        <div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Username">
                                        <input type="text" class="form-control mb-3" placeholder="Username">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Name">
                                        <input type="text" class="form-control mb-3" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Email">
                                        <input type="email" class="form-control mb-3" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Phone">
                                        <input type="number" class="form-control mb-3" placeholder="Phone">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Organization">
                                        <select class="custom-select mb-3">
                                            <option>Organization</option>
                                            <option value="1">PT Armadius Teknologi</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Gender">
                                        <select class="custom-select mb-3">
                                            <option>Gender</option>
                                            <option value="1">Man</option>
                                            <option value="2">Woman</option>
                                            <option value="3">Animal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Date of birth">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Date of birth" data-provide="datepicker">
                                            <div class="input-group-append">
                                                <span class="input-group-text append-white"><i data-feather="calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Photo profile">
                                        <div class="custom-file mb-3">
                                        <input type="file" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="formLabel" data-tooltip="Address">
                                        <textarea class="form-control" id="address" rows="3">Jl Jendral Sudirman, Jakarta pusat 14045</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="#" class="btn btn-sm btn-outline-primary mr-2">Cancel</a>
                        <a href="#" id="saveData" class="btn btn-sm btn-primary">Save event</a>
                    </div>
                </div>
                </form>
            </div>

            

        </div>

    </div>
    <!-- Main Content -->

</div>
<!-- Layout -->
<?php include_once "footer.php"; ?>